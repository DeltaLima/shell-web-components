import {Component, Input} from '@angular/core';

@Component({
  /* selector: 'app-root', // No need for the selector because this is define in app.module */
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  /* encapsulation: ViewEncapsulation.Native */
})
export class AppComponent {
  @Input() title = 'app';
}
