import { BrowserModule } from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {createCustomElement} from '@angular/elements';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: [
    AppComponent
  ]
})
export class AppModule {

  constructor(private injector: Injector) {
  }

  /**
   * Here you define your web component element
   * Don't forget to add the AppComponent in the entryComponents array
   * @type {NgElementConstructor<any>}
   */
  ngDoBootstrap() {
    const wcPlayground = createCustomElement(AppComponent, { injector: this.injector });
    customElements.define('wc-playground', wcPlayground);
  }
}
