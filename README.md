# Angular Elements UX Zurich Demo

Shows how to dynamically load and create Angular Elements (Custom Elements, Web Components).

![Show Case](./img.png)

Features:
- Dynamically create app-internal Angular Elements
- Load external Custom Elements

Remarks:
- The external Custom Element is built with [ngx-build-plus](https://www.npmjs.com/package/ngx-build-plus)
- The example uses the ``@webcomponents/custom-elements`` polyfill (see references in ``angular.json`` and ``polyfills.ts``)
- External Components will become much smaller when ngIvy arrives (planned for Angular 7)
- For sharing dependencies b/w components, lookup [ngx-build-plus](https://www.npmjs.com/package/ngx-build-plus)

## Start

Build the external Custom Elements project before starting the main project:

```
npm install
npm run build:wc-playground
npm start
```

## Overview
The main project is define in angular.json, in this example the only purpose for the main project is to load the webcomponents (shell),
Here we are using angular framework but as web components are standard, it could be another framework.
You can check in the Google Inspector that the web components is built in Angular 6.

To be able to run web components in angular Framework, we must :

- Install web-components package :
```
npm i @webcomponents/custom-elements
```

- Update the polyfills.ts :

```
import '@webcomponents/custom-elements/custom-elements.min';
import '@webcomponents/custom-elements/src/native-shim' // (for < angular 6)
```

- Add CUSTOM_ELEMENTS in app.module :
```
schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
```

This setup is already done for this demo.

## Setting up your own custom web component
### Creation of project
This command generate a brand new application in projects folder where you can manage one or multiple webcomponents
```
ng generate application {name}
```
### Updates

Files to be aligned with wc-playground example: index.html, app.module, app.component.ts

### Builder configuration ( It will be simplified in the future thanks to angular 7) 

In angular.json, when you create a new "sub" application, the command updates automatically angular.json file
Here we need to adjust these configuration options (mainly for the builder) to able to create a web component

Wc-Playground example :
```
 "wc-playground": {
      "root": "projects/wc-playground/",
      "sourceRoot": "projects/wc-playground/src",
      "projectType": "application",
      "prefix": "zic",
      "schematics": {},
      "architect": {
        "build": {
          "builder": "ngx-build-plus:build", // USE THIS CUSTOM BUILDER
          "options": {
            "outputPath": "dist/wc-playground",
            "index": "projects/wc-playground/src/index.html",
            "main": "projects/wc-playground/src/main.ts",
            "polyfills": "projects/wc-playground/src/polyfills.ts",
            "tsConfig": "projects/wc-playground/tsconfig.app.json",
            "assets": [
              "projects/wc-playground/src/favicon.ico",
              "projects/wc-playground/src/assets"
            ],
            "styles": [
              "projects/wc-playground/src/styles.css"
            ],
            "scripts": [
              "node_modules/@webcomponents/custom-elements/src/native-shim.js" // TO RESOLVE SOME COMPATIBLITY (SHOULD DE ADDED IN MAIN PROJECT TOO)
            ]
          },
          "configurations": {
            "local": {           // OPTIONAL : TO RUN LOCALLY DEPENDING ON EACH ENVIRONMENT (see npm scripts)
              "fileReplacements": [
                {
                  "replace": "projects/wc-playground/src/environments/environment.ts",
                  "with": "projects/wc-playground/src/environments/environment.local.ts"
                }
              ],
              "optimization": false
            },
            "dev": {
              "fileReplacements": [
                {
                  "replace": "projects/wc-playground/src/environments/environment.ts",
                  "with": "projects/wc-playground/src/environments/environment.dev.ts"
                }
              ],
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": false,
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true
            },
            "production": {
              "fileReplacements": [
                {
                  "replace": "projects/wc-playground/src/environments/environment.ts",
                  "with": "projects/wc-playground/src/environments/environment.prod.ts"
                }
              ],
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": false,
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true
            }
          }
        },
        "serve": {
          "builder": "@angular-devkit/build-angular:dev-server",
          "options": {
            "browserTarget": "wc-playground:build"
          },
          "configurations": {
            "local": {
              "browserTarget": "wc-playground:build:local"
            },
            "production": {
              "browserTarget": "wc-playground:build:production"
            }
          }
        },
        "extract-i18n": {
          "builder": "@angular-devkit/build-angular:extract-i18n",
          "options": {
            "browserTarget": "wc-playground:build"
          }
        },
        "test": {
          "builder": "@angular-devkit/build-angular:karma",
          "options": {
            "main": "projects/wc-playground/src/test.ts",
            "polyfills": "projects/wc-playground/src/polyfills.ts",
            "tsConfig": "projects/wc-playground/tsconfig.spec.json",
            "karmaConfig": "projects/wc-playground/karma.conf.js",
            "styles": [
              "projects/wc-playground/src/styles.css"
            ],
            "scripts": [],
            "assets": [
              "projects/wc-playground/src/favicon.ico",
              "projects/wc-playground/src/assets"
            ]
          }
        },
        "lint": {
          "builder": "@angular-devkit/build-angular:tslint",
          "options": {
            "tsConfig": [
              "projects/wc-playground/tsconfig.app.json",
              "projects/wc-playground/tsconfig.spec.json"
            ],
            "exclude": [
              "**/node_modules/**"
            ]
          }
        }
      }
    },
```

NPM Commands

To run locally your webcomponent in a standalone way based on local environment file
```
    "start:wc-playground": "ng serve --project --configuration=local wc-playground"
```

To build the webcomponent and copy the result to assets folder in main project
```
    "build:wc-playground": "npm run bundle:wc-playground && npm run copy:wc-playground",
    "copy:wc-playground": "cpr dist/wc-playground/main.js src/assets/wc-playground.bundle.js -o",
    "bundle:wc-playground": "ng build --project wc-playground --prod --output-hashing none"
```

## Useful links
- Web Components : https://www.webcomponents.org/
- Angular Elements: https://angular.io/guide/elements
- MDN : https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
- W3C: https://www.w3.org/standards/techs/components#w3c_all







