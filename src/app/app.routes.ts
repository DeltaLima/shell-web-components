import { HomeComponent } from './home/home.component';
import { Routes } from "@angular/router";
import {WebComponentsComponent} from './web-components/web-components.component';

export const APP_ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'web-component',
        component: WebComponentsComponent
    },
    {
        path: '**',
        redirectTo: 'home'
    }
];
