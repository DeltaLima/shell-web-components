import { Component, OnInit } from '@angular/core';
import {WebComponentsService} from './web-components.service';

@Component({
  selector: 'app-web-components',
  templateUrl: './web-components.component.html',
  styleUrls: ['./web-components.component.css'],
  providers: [ WebComponentsService ]
})
export class WebComponentsComponent implements OnInit {

  constructor(private webComponentsService: WebComponentsService) { }

  ngOnInit() {
    this.loadWebComponent('wc-playground');
  }


  loadWebComponent(name: string): void {
    this.webComponentsService.load(name);
    this.add(name);
  }

  add(webcomponentHtmlTagName: string): void {

    const data = ['HELLO I AM A WEB COMPONENT' ];

    const tile = document.createElement(webcomponentHtmlTagName);
    tile.setAttribute('class', 'col-lg-12 col-md-8 col-sm-4');
    tile.setAttribute('title', '' + data[0]);

    const content = document.getElementById('content');
    content.appendChild(tile);

  }

}
