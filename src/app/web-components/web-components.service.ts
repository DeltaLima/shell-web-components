import { Injectable } from '@angular/core';

@Injectable()
export class WebComponentsService {

  constructor() { }

  loaded = false;

  load(name: string): void {
    if (this.loaded) return;
    const script = document.createElement('script');
    script.src = `assets/${name}.bundle.js`;
    document.body.appendChild(script);
    this.loaded = true;
  }

}
